package com.example.sergo.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.TextureView;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Integer countOfStudents = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickAddButttonStudent(View view) {
        countOfStudents++;
        TextView counterView = (TextView)findViewById(R.id.txt_counter);
        counterView.setText(countOfStudents.toString());
    }
}
