package com.example.sergo.table;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.TextureView;
import android.view.View;
import android.widget.TextView;

public class Table extends AppCompatActivity {

    private Integer goal1 = 0;
    private Integer goal2 = 0;

    TextView zen;
    TextView spar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table);
        zen = (TextView) findViewById(R.id.btn_zenit);
                spar = (TextView) findViewById(R.id.btn_spartak);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        goal1 = savedInstanceState.getInt("btn_zenit");
        goal2 = savedInstanceState.getInt("btn_spartak");
        zen.setText(goal1.toString());
        spar.setText(goal2.toString());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("btn_zenit",goal1);
        outState.putInt("btn_spartak",goal2);
    }


    public void onClickAddButttonLeft(View view) {
        goal1++;
        TextView counterView = (TextView)findViewById(R.id.btn_zenit);
        counterView.setText(goal1.toString());
    }

    public void onClickAddButttonRight(View view) {
        goal2++;
        TextView counterView = (TextView)findViewById(R.id.btn_spartak);
        counterView.setText(goal2.toString());
    }
}