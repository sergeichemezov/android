package com.example.admin.svetofor;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import static android.R.id.button2;

public class MainActivity extends AppCompatActivity {

    private LinearLayout Mactivity_main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Mactivity_main = (LinearLayout) findViewById(R.id.Mactivity_main);
    }

    public void onClickRed (View view){

        Mactivity_main.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorRed));

    }

    public void onClickYellow (View view){

        Mactivity_main.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorYellow));;

    }

    public void onClickGreen (View view){

        Mactivity_main.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorGreen));

    }
}
